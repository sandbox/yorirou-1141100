<?php

function hosting_domain_suffix_hosting_feature() {
  $features = array();

  $features['domain_suffix'] = array(
    'title' => t('Domain suffix'),
    'description' => t('Adds a domain suffix to the site domain'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_domain_suffix',
    'group' => 'experimental',
  );

  return $features;
}
